package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.javafx.scene.control.skin.TextFieldSkin;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import static settings.AppPropertyType.*;

/**
 * @author Ritwik Banerjee, Emerson Biro
 */

public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private TextField[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     isCanceled = false;
    private Path        workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            gameButton = ws.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        ws.getHintButton().setOnAction(e->{
            gamedata.setHasHint(false);
            gamedata.setUsedHint(true);

            ArrayList <Character> list = new ArrayList<>(gamedata.getGoodGuesses());

            Set<Character> set = new LinkedHashSet();

            for (char c:gamedata.getTargetWord().toCharArray()) {
                set.add(Character.valueOf(c));
            }

            ArrayList <Character> uniqueLetters = new ArrayList<>(set);

            for (int i = 0; i < list.size(); i++) {
                for (int j = 0; j < uniqueLetters.size(); j++) {
                    if(list.get(i) == uniqueLetters.get(j)) {
                        uniqueLetters.remove(list.get(i));
                    }
                }
            }

            char randomChar = uniqueLetters.get((int)(Math.random() * (uniqueLetters.size())));

            for (int i = 0; i < progress.length; i++) {
                if (gamedata.getTargetWord().charAt(i) == randomChar) {
                    progress[i].setText(Character.toString(randomChar));
                    progress[i].setStyle("-fx-control-inner-background: white");
                    letterField[((int)randomChar-97)].setStyle("-fx-control-inner-background: red");
                    gamedata.addGoodGuess(randomChar);
                    discovered++;
                    appTemplate.getGUI().updateWorkspaceToolbar(true);
                    isAlreadySaved = false;
                }
            }

            gamedata.setRemainingGuesses(gamedata.getRemainingGuesses() -1);
            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
            ws.getHintButton().setVisible(false);
        });


        ws.getHintButton().setVisible(false);

        // if the game was not loaded, create a new random word
        if (loaded == false) {

            createWord();
            checkWord();
        }
        // makes sure that the json file it self is not corrupted or something
        if (gamedata.getTargetWord() !=null) {

            if(gamedata.isHasHint() == true) {
                ws.getHintButton().setVisible(true);
                ws.getHintButton().setDisable(false);
            }

            gameover = false;
            success = false;
            savable = true;
            discovered = 0;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            HBox remainingGuessBox = ws.getRemainingGuessBox();
            HBox guessedLetters    = (HBox) ws.getGameTextsPane().getChildren().get(1);

            if (loaded == false) {
                remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
            } else {
                remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
            }
            remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);


            initWordGuessed(ws.getLettersGuessedBox());
            initWordGraphics(guessedLetters);
            if (gamedata != null) {
                play();
            }
        } else {
            popUp(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE), null);
            gameButton.setDisable(false);
        }
    }

    private void createWord() {
        boolean badWord = false;

        do {
            GameData temp = new GameData(appTemplate);
            for (char c : temp.getTargetWord().toCharArray()) {
                if(!Character.isLetter(c)) {
                    badWord = true;
                }
            }

            if(badWord == false) {
                gamedata = temp;
            }

        } while (badWord == true);

    }

    private void checkWord() {
        String word = gamedata.getTargetWord();
        String temp = "";

        for (int i = 0; i < word.length(); i++) {
            if (temp.indexOf(word.charAt(i)) == -1) {
                temp = temp + word.charAt(i);
            }
        }

        if(temp.length() > 7){
            gamedata.setHasHint(true);
            gamedata.setUsedHint(false);
        } else {
            gamedata.setHasHint(false);
            gamedata.setUsedHint(true);
        }

    }


    public void popUp(String title, String header, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);

        alert.show();
    }

    public void conformationDialog(String title, String header, String message) {
        isCanceled = false;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);

        ButtonType yes = new ButtonType("Yes");
        ButtonType no = new ButtonType("No");
        ButtonType cancel = new ButtonType("Cancel");

        alert.getButtonTypes().setAll(yes, no, cancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == yes){
            try {
                handleSaveRequest();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(result.get() == no) {

        } else {
            isCanceled = true;
        }
    }

    private void end() {
        if (gameButton.isDisable() == true) {
            if (success) {
                popUp("Game Over", "YOU WIN!", "You figured out the word, congratulations!");
                isCanceled = false;
            } else {
                popUp("Game Over", "Better luck next time!", null);
                isCanceled = false;
            }
        }
        ws.getHintButton().setVisible(false);
        loaded = false;
        gamedata.reset();
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;

        gameButton.setDisable(false); //changed the disable to false, so when the game is over it makes the start playing able to be clicked again.

        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();

        progress = new TextField[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new TextField();
            progress[i].setEditable(false);
            progress[i].setMouseTransparent(true);
            progress[i].setFocusTraversable(false);
            progress[i].setStyle("-fx-control-inner-background: black");
            progress[i].setMinWidth(24); progress[i].setMaxWidth(24);

        }
        guessedLetters.getChildren().addAll(progress);
    }

    private  TextField[] letterField;

    private void initWordGuessed(TilePane guessedLetters) {
        letterField = new TextField[26];

        for (int i = 0; i < letterField.length; i++) {
            letterField[i] = new TextField();
            letterField[i].setFont(Font.font("Verdana", FontWeight.BOLD, 10));
            letterField[i].setText(Character.toString((char)(65 + i)));
            letterField[i].setEditable(false);
            letterField[i].setMouseTransparent(true);
            letterField[i].setFocusTraversable(false);
            letterField[i].setStyle("-fx-control-inner-background: #43f019");
            letterField[i].setMinWidth(40); letterField[i].setMaxWidth(40);
            letterField[i].setMinHeight(40); letterField[i].setMaxHeight(40);

        }
        guessedLetters.getChildren().addAll(letterField);
    }


    private Workspace ws;


    public void play() {

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {


                if (b) {
                    b = false;
                    ArrayList <Character> list = new ArrayList<>(gamedata.getGoodGuesses());

                    for(int i = 0; i < list.size(); i++) {
                        for (int j = 0; j < progress.length; j++) {
                            if (gamedata.getTargetWord().charAt(j) == list.get(i)) {
                                progress[j].setText(Character.toString(gamedata.getTargetWord().charAt(j)));
                                progress[j].setStyle("-fx-control-inner-background: white");
                                letterField[((int)list.get(i)-97)].setStyle("-fx-control-inner-background: red");
                                discovered++;
                            }

                        }
                    }

                    ArrayList <Character> bg = new ArrayList<>(gamedata.getBadGuesses());
                    for (int j = 0; j < bg.size(); j++) {
                        letterField[((int)bg.get(j)-97)].setStyle("-fx-control-inner-background: red");
                    }
                }

                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    String stringGuess = event.getCharacter().toLowerCase();

                    if (Character.isLetter(stringGuess.charAt(0))) {
                        char guess = event.getCharacter().toLowerCase().charAt(0);
                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                try {
                                    if (gamedata.getTargetWord().charAt(i) == guess) {
                                        progress[i].setText(Character.toString(gamedata.getTargetWord().charAt(i)));
                                        progress[i].setStyle("-fx-control-inner-background: white");
                                        letterField[((int)guess-97)].setStyle("-fx-control-inner-background: red");
                                        gamedata.addGoodGuess(guess);
                                        goodguess = true;
                                        discovered++;
                                        appTemplate.getGUI().updateWorkspaceToolbar(true);
                                        isAlreadySaved = false;
                                    }
                                } catch (Exception e) {}
                            }
                            if (!goodguess) {
                                gamedata.addBadGuess(guess);
                                letterField[((int)guess-97)].setStyle("-fx-control-inner-background: red");
                                appTemplate.getGUI().updateWorkspaceToolbar(true);
                            }
                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                            ws.hangmanGraphic(gamedata.getRemainingGuesses());
                            isAlreadySaved = false;
                        }
                    }
                });

                if(success) {
                    displayEnd();
                    stop();
                }

                if (gamedata.getRemainingGuesses() <= 0) {
                    displayEnd();
                    stop();
                }

                if (gameButton.isDisable() != true && gameover !=true) {
                    displayEnd();
                    stop();
                }

                if(savable == false) {
                    stop();
                }

            }

            @Override
            public void stop() {
                super.stop();
                end();
            }

        };
        timer.start();
    }

    private void displayEnd() {
        ArrayList <Character> list = new ArrayList<>(gamedata.getGoodGuesses());

        for (int i = 0; i < progress.length; i++) {
            if(progress[i].getText().isEmpty()) {
                progress[i].setText(Character.toString(gamedata.getTargetWord().charAt(i)));
                progress[i].setStyle("-fx-control-inner-background: red");
            } else {
                progress[i].setStyle("-fx-control-inner-background: green");
            }
        }

    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    private boolean     savable = false;
    private boolean     loaded;
    private boolean     isAlreadySaved = false;
    private boolean     b = true;
    private boolean     alreadyExecuted = false;

    @Override
    public void handleNewRequest() {
        ws = (Workspace) appTemplate.getWorkspaceComponent();

        // made so the button only initializes once
        if (!alreadyExecuted) {
            enableGameButton();
            alreadyExecuted = true;
        }

        // if the start playing button is enabled dont ask for user to save.
        if(gameButton.isDisable() == true && !isAlreadySaved) {
            conformationDialog(props.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), props.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE), null);

        }

        if (!isCanceled) {

            isAlreadySaved = false;

            ws.getHintButton().setVisible(false);
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            gameButton.setDisable(false);

            savable = false;

            loaded = false;
            isAlreadySaved = false;
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated

            ws.reinitialize();
        }
    }

    private PropertyManager props  = PropertyManager.getManager();

    @Override
    public void handleSaveRequest() throws IOException {
        // if the game was loaded and user saves again it will save it to the same file.
        if(loaded == true && selectedLoadFile != null){
            isCanceled = false;
            isAlreadySaved = true;
            ObjectMapper mapper = new ObjectMapper();

            mapper.writerWithDefaultPrettyPrinter().writeValue(selectedLoadFile, gamedata);
            appTemplate.getGUI().updateWorkspaceToolbar(false);


            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(props.getPropertyValue(SAVE_COMPLETED_TITLE));
            alert.setHeaderText(props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
            alert.setContentText(null);

            alert.showAndWait();

        }

        // if the game was never saved before prompt user to save a file
        if (loaded != true) {
            isCanceled = false;
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(props.getPropertyValue(SAVE_WORK_TITLE));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("json files (*.json)", "*.json");
            fileChooser.getExtensionFilters().add(extFilter);
            File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());

            // make sure the selected file exist before trying to save
            if (selectedFile != null) {
                isAlreadySaved = true;
                loaded = true;
                ObjectMapper mapper = new ObjectMapper();

                mapper.writerWithDefaultPrettyPrinter().writeValue(selectedFile, gamedata);

                appTemplate.getGUI().updateWorkspaceToolbar(false);
                selectedLoadFile = selectedFile;
                popUp(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE), null);
            }
        }
    }

    File selectedLoadFile;
    @Override
    public void handleLoadRequest() {
        ws = (Workspace) appTemplate.getWorkspaceComponent();

        if (!alreadyExecuted) {
            enableGameButton();
            alreadyExecuted = true;
        }

        if (!isAlreadySaved && gameButton != null && gameButton.isDisable() == true) {
            conformationDialog(props.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), props.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE), null);
        }


        if (!isCanceled) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(props.getPropertyValue(LOAD_WORK_TITLE));
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("json files (*.json)", "*.json");
            fileChooser.getExtensionFilters().add(extFilter);
            selectedLoadFile = fileChooser.showOpenDialog(appTemplate.getGUI().getWindow());

            //checks to see if the file loaded exist, and or is of the type json
            if (selectedLoadFile != null && selectedLoadFile.getName().endsWith(".json")) {
                b = true;                                              //makes sure the program puts the words guessed good, into the gui
                appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
                appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                ensureActivatedWorkspace();                            // ensure workspace is activated

                ws.reinitialize();
                enableGameButton();
                gameButton.setDisable(true);

                //if all goes good up to this point, then the json file is read and loaded into gamedata, and starts the game.
                ObjectMapper mapper = new ObjectMapper();

                try {
                    GameData temp = mapper.readValue(selectedLoadFile, GameData.class);

                    boolean badWord = false;
                    for (char c : temp.getTargetWord().toCharArray()) {
                        if(!Character.isLetter(c)) {
                            badWord = true;
                        }
                    }

                    if ((temp.getRemainingGuesses() >= 0 && temp.getTargetWord() != null && temp.getGoodGuesses() != null && temp.getBadGuesses() != null)
                            && ((temp.isHasHint() == true && temp.isUsedHint() == false) ||(temp.isHasHint() == false && temp.isUsedHint() == true)) && badWord == false) {
                        isAlreadySaved = true;

                        gamedata = temp;
                        ws.hangmanGraphic(gamedata.getRemainingGuesses());
                        loaded = true;


                        start();
                        appTemplate.getGUI().updateWorkspaceToolbar(false);
                    } else {
                        popUp(props.getPropertyValue(LOAD_ERROR_TITLE), props.getPropertyValue(LOAD_ERROR_MESSAGE), null);
                        gameButton.setDisable(false);
                    }

                } catch (IOException e) {
                    popUp(props.getPropertyValue(LOAD_ERROR_TITLE), props.getPropertyValue(LOAD_ERROR_MESSAGE), null);
                    gameButton.setDisable(false);
                }
            } else if (selectedLoadFile == null) {
                //just do nothing if you "X" out or close...
            } else if (!selectedLoadFile.getName().endsWith(".json")){
                popUp(props.getPropertyValue(LOAD_ERROR_TITLE), props.getPropertyValue(LOAD_ERROR_MESSAGE), null);
                gameButton.setDisable(false);
            }
        }
    }

    @Override
    public void handleExitRequest() {
        if (!isAlreadySaved && gameButton != null && gameButton.isDisable() == true) {
            conformationDialog(props.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), props.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE), null);
        }

        if (!isCanceled) {
            System.exit(0);
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }


}